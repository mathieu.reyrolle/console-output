# Console Output

Use of Windows 32-bit DLL to generate standard output and/or error from a LabVIEW application, if run from command line.
This code is largely based on content of the discussion from https://lavag.org/topic/11719-running-a-labview-exe-from-the-console/?page=2

Three way of handling standard output in your application are proposed :
 - with an action engine, that exposes four actions (Init, StdOut, StdErr, Close)
 - with a class that exposes four methods (Initialize, Write StdOut, Writs StdErr, Release)
 - with a DQMH module instance (DQMH v6.0.083), that uses the class, handle internally opening and releasing ressources, and that exposes two Request in Public API, Write Line to StdOut and Write Line to StdErr

The three corresponding projects have a build specification in order to get a quick ready torun example of theses implementaions. Feel free to choose the one you prefer, or to adapt it to your needs !

Environnement : LabVIEW 2015

Limitattions :
 - This code is based on 32-bit Windows DLL. It is only supposed to works with LabVIEW 32-bit.
 - When launching from cmd, the invoque line returns almost immediatly, and then the outputs (std and err) are displayed by the terminal, producing something wreid when testing executables "manually" (I mean, I doesn't expect this behavior). Otherwise, standard output and standard error are correctly get when using a batch script, CallExec.vi in LabVIEW or Call Executable step in TestStand: it waits the end of the executable execution to retrieve all standard output contents.